KDIR ?= /lib/modules/`uname -r`/build

obj-m += net_drv.o

all:
	make -C $(KDIR) M=$(PWD) modules
clean:
	make -C $(KDIR) M=$(PWD) clean

